# Adding new storage capacity

When Gitaly storage reaches a saturation point it will be necessary to increase capacity by adding new instances.
This is done with Change Management, using the `/change declare` command in Slack and selecting the `change_gitaly_storage_creation.md` template.
